#!/bin/bash

# by tansi

export P_PORT=8080
export P_NAME=ROOT.war

case $1 in
    
    "start")
        nohup java -jar "$P_NAME" >nohup.out 2>&1 &
	START_PORT=`lsof -i:"$P_PORT" | grep "LISTEN" | awk '{ print $2 }'`
	until [ -n "$START_PORT" ]
	do
		START_PORT=`lsof -i:"$P_PORT" | grep "LISTEN" | awk '{ print $2 }'`
	done
	echo "pid: $START_PORT"
        if [ -n "$START_PORT" ];then
            echo "$P_NAME,pid:$START_PORT is started"
        fi

    ;;

    "stop")
	    PORT=`ps -ef | grep -w "$P_NAME" | grep -v "grep" | awk '{ print $2 }'`
		if [ -n "$PORT" ];then
			kill -9 "$PORT"
			echo "$P_NAME success stop"
		else
			echo "process not exist or stop success!"
		fi    
    ;;

    "restart")
    ;;

    *)
	echo "Usage: xx.sh [start|stop]"
    	echo "error,please input command!"
    ;;


esac
