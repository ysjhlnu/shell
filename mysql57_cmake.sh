#!/bin/bash
# by tansi 2021/06/24


cd /tmp

if [[ ! -f "mysql-5.7.33.tar.gz" ]];then

fi


if [[ ! -f "boost_1_59_0.tar.gz" ]];then
	
fi

tar -xf mysql-5.7.33.tar.gz
tar -xf boost_1_59_0.tar.gz 

groupadd mysql
useradd -d /home/mysql -g mysql -m mysql


mkdir /usr/local/mysql{data,log,run} -p
chown -R mysql.mysql /usr/local/mysql/

yum install cmake gcc gcc-c++ ncurses-devel bison zlib libxml openssl automake autoconf make libtool bison-devel libaio-devel openssl* -y


cd /tmp/mysql-5.7.33
cmake \
-DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
-DMYSQL_DATADIR=/usr/local/mysql/data \
-DSYSCONFDIR=/etc \
-DWITH_BOOST=/tmp/boost_1_59_0 \
-DWITH_MYISAM_STORAGE_ENGINE=1 \
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
-DWITH_MEMORY_STORAGE_ENGINE=1 \
-DWITH_READLINE=1 \
-DMYSQL_UNIX_ADDR=/var/lib/mysql/mysql.sock \
-DMYSQL_TCP_PORT=3306 \
-DENABLED_LOCAL_INFILE=1 \
-DWITH_PARTITION_STORAGE_ENGINE=1 \
-DEXTRA_CHARSETS=all \
-DDEFAULT_CHARSET=utf8 \
-DDEFAULT_COLLATION=utf8_general_ci
#可以使用echo $?，如果返回0代表OK无异常。

make && make install
#也可以分开一步一步来，make完成后通过echo $?确认OK后再执行make install。

cat >> /etc/my.cnf <<EOF
[mysqld]
user=mysql
basedir=/usr/local/mysql/
datadir=/usr/local/mysql/data
socket=/tmp/mysql.sock
server_id=6
port=3306

[mysql]
socket=/tmp/mysql.sock
EOF

/usr/local/mysql/bin/mysqld --initialize-insecure --user=mysql --basedir=/usr/local/mysql/ --datadir=/usr/local/mysql/data/

cat >/etc/systemd/system/mysqld.service <<EOF
[Unit]
Description=MySQL Server
After=network.target
After=syslog.target

[Install]
WantedBy=multi-user.target

[Service]
User=mysql
Group=mysql
ExecStart=/usr/local/mysql/bin/mysqld --defaults-file=/etc/my.cnf
LimitNOTILE=5000
EOF


systemctl start mysqld
systemctl status mysqld


vim /etc/profile
export PATH=/usr/local/mysql/bin:$PATH
source /etc/profile